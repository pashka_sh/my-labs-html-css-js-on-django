
from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = "home.html"

class CalculateView(TemplateView):
    template_name = "calculate.html"

class DateView(TemplateView):
    template_name = "date.html"

class QuadroView(TemplateView):
    template_name = "quadro.html"

class OrganizationView(TemplateView):
    template_name = "organization.html"